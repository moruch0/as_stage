import 'package:as_stage/constans/app_colors.dart';
import 'package:flutter/material.dart';

ThemeData basicTheme() => ThemeData(
  brightness: Brightness.light,
  primaryColor: AppColors.lightPrimary,

  textTheme:  const TextTheme(
    headline6: TextStyle(
      color: AppColors.baseColor,
    )
  )
);