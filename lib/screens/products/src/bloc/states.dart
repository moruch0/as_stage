part of 'bloc_products.dart';



abstract class StateBlocProducts {}

class StateProductsInitial extends StateBlocProducts {}

class StateProductsLoading extends StateBlocProducts {
  final bool isLoading;

  StateProductsLoading(this.isLoading);

}

class StateProductsData extends StateBlocProducts {
  StateProductsData({
    required this.data,
  });

  final List<ProductModel> data;
}

class StateCategoriesData extends StateBlocProducts {
  StateCategoriesData({
    required this.data,
  });

  final List<String> data;
}

class StateProductsError extends StateBlocProducts {
  StateProductsError(this.error);

  final String error;
}
