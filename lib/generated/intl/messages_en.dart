// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "authorization": MessageLookupByLibrary.simpleMessage("Authorization"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "counter_value": MessageLookupByLibrary.simpleMessage("Counter value:"),
        "create": MessageLookupByLibrary.simpleMessage("Create"),
        "dont_have_account": MessageLookupByLibrary.simpleMessage(
            "Don\'t you have an account yet?"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enter_login": MessageLookupByLibrary.simpleMessage("Enter login"),
        "enter_login_and_pass":
            MessageLookupByLibrary.simpleMessage("Enter login and password"),
        "enter_the_password":
            MessageLookupByLibrary.simpleMessage("Enter the password"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "home_page": MessageLookupByLibrary.simpleMessage("Home page"),
        "invalid_username_or_password_entered":
            MessageLookupByLibrary.simpleMessage(
                "Invalid username or password entered"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "login_must_contain_least_3_characters":
            MessageLookupByLibrary.simpleMessage(
                "Login must contain least at 3 characters"),
        "ok": MessageLookupByLibrary.simpleMessage("Ок"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "password_must_contain_least_8_characters":
            MessageLookupByLibrary.simpleMessage(
                "Password must contain least at 8 characters"),
        "product_details":
            MessageLookupByLibrary.simpleMessage("Product Details"),
        "products": MessageLookupByLibrary.simpleMessage("Products"),
        "russian": MessageLookupByLibrary.simpleMessage("Russian"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Something went wrong "),
        "try_again": MessageLookupByLibrary.simpleMessage("Try again"),
        "username": MessageLookupByLibrary.simpleMessage("Username")
      };
}
