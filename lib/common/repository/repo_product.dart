import 'dart:convert';

import 'package:as_stage/common/models/response/products_model.dart';
import 'package:as_stage/common/repository/api.dart';

class RepoProduct {
  final Api api;

  RepoProduct({required this.api});

  Future<ResultRepoProduct> getProducts({required String sort}) async {
    try {
      final response = await api.dio.get('products?sort=$sort');
      final List productsJson = response.data;
      final products = productsJson
          .map(
            (item) => ProductModel.fromJson(item),
          )
          .toList();
      return ResultRepoProduct(products: products);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoProduct(errorMesage: 'erro');
    }
  }

  Future<ResultRepoProduct> getCategorieProducts(
      {required String categorie, required String sort}) async {
    try {
      final response =
          await api.dio.get('products/category/$categorie?sort=$sort');

      final List productsJson = response.data;

      final products = productsJson
          .map(
            (item) => ProductModel.fromJson(item),
          )
          .toList();

      return ResultRepoProduct(products: products);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoProduct(errorMesage: 'error');
    }
  }

  Future<ResultRepoCategories> getAllCategories() async {
    try {
      final response = await api.dio.get('products/categories');

      final List categoriesJson = response.data;

      List<String> categories = [];

      for (var item in categoriesJson) {
        if (item is String) {
          categories.add(item);
        }
      }

      return ResultRepoCategories(categories: categories);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoCategories(errorMesage: 'error');
    }
  }
}

class ResultRepoProduct {
  ResultRepoProduct({
    this.errorMesage,
    this.products,
  });

  final String? errorMesage;
  final List<ProductModel>? products;
}

class ResultRepoCategories {
  ResultRepoCategories({
    this.errorMesage,
    this.categories,
  });

  final String? errorMesage;
  final List<String>? categories;
}
