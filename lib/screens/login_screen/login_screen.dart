import 'package:as_stage/common/widgets/app_nav_bar/app_nav_bar.dart';
import 'package:as_stage/constans/app_assets.dart';
import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/constans/app_styles.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String? login;
  String? password;

  @override
  Widget build(BuildContext context) {
    var _local = S.of(context);
    return Scaffold(
      backgroundColor: AppColors.lightPrimary,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 28),
        child: Column(
          children: [
            const SizedBox(height: 20),
            Expanded(
              child: Image.asset(
                AppAssets.images.logo,
                width: 2000,
                height: 2000,
              ),
            ),
            Column(
              children: [
                const SizedBox(height: 25),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        _local.username,
                        style: AppStyles.label,
                      ),
                      const SizedBox(height: 8),
                      _CustomInput(
                        assetName: AppAssets.svg.account,
                        text: _local.username,
                        // maxLength: 8,
                        validator: (value) {
                          if (value == null || value == "") {
                            return null;
                          }
                          if (value.length < 3) {
                            return null;
                          }
                          return null;
                        },
                        onChanged: (value) {
                          login = value;
                        },
                      ),
                      const SizedBox(height: 10),
                      Text(
                        _local.password,
                        style: AppStyles.label,
                      ),
                      const SizedBox(height: 8),
                      _CustomInput(
                        assetName: AppAssets.svg.password,
                        isPassword: true,
                        text: _local.password,
                        suffixIcon: SvgPicture.asset(
                          AppAssets.svg.obscure,
                          height: 34,
                          width: 34,
                          fit: BoxFit.scaleDown,
                          color: AppColors.background,
                        ),

                        // maxLength: 16,
                        validator: (value) {
                          if (value == null || value == "") {
                            return null;
                          }
                          if (value.length < 8) {
                            return null;
                          }
                          return null;
                        },
                        obscureText: true,
                        onChanged: (value) {
                          password = value;
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 24),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            AppColors.darkBlue),
                        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          const EdgeInsets.symmetric(vertical: 12),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      onPressed: () {
                        final isValidated =
                            _formKey.currentState?.validate() ?? false;

                        if (isValidated) {
                          FocusScope.of(context).unfocus();
                          if (login == "qwerty" && password == "123456ab") {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (contextRoute) {
                                return const AppNavBar();
                              },
                            ));
                          } else {
                            _dialog();
                          }
                        } else {
                          _dialog();
                        }
                      },
                      child: Text(_local.login)),
                ),
                const SizedBox(height: 24),
                const SizedBox(height: 28),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _dialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).error,
              style: AppStyles.mediumText.copyWith(fontSize: 20),
            ),
            const SizedBox(height: 20),
            Text(S.of(context).invalid_username_or_password_entered,
                style: AppStyles.label.copyWith(fontSize: 13)),
          ],
        ),
        actions: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            width: double.infinity,
            child: ElevatedButton(
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(0),
                backgroundColor:
                    MaterialStateProperty.all<Color>(AppColors.darkBlue),
                padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                  const EdgeInsets.symmetric(vertical: 12),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
              onPressed: () => Navigator.pop(context),
              child: Text(
                S.of(context).ok,
                style:
                    AppStyles.primaryText.copyWith(color: AppColors.background),
              ),
            ),
          ),
          const SizedBox(height: 15),
        ],
      ),
    );
  }
}

class _CustomInput extends StatefulWidget {
  final String text;
  final String? Function(String?)? validator;
  final int? maxLength;
  final bool obscureText;
  final String? assetName;
  final void Function(String)? onChanged;
  final Widget? suffixIcon;
  final bool isPassword;

  const _CustomInput({
    Key? key,
    required this.text,
    this.maxLength,
    this.validator,
    this.onChanged,
    this.assetName,
    this.suffixIcon,
    this.obscureText = false,
    this.isPassword = false,
  }) : super(key: key);

  @override
  State<_CustomInput> createState() => __CustomInputState();
}

class __CustomInputState extends State<_CustomInput> {
  late bool _obscureText;
  @override
  void initState() {
    super.initState();
    _obscureText = widget.obscureText;
  }

  @override
  Widget build(BuildContext context) {
    var styelBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(12),
      borderSide: const BorderSide(color: AppColors.background, width: 0.0),
    );
    return TextFormField(
      cursorColor: AppColors.background,
      style: AppStyles.mediumText.copyWith(color: AppColors.background),
      decoration: InputDecoration(
        focusedBorder: styelBorder,
        enabledBorder: styelBorder,
        prefixIcon: widget.assetName == null
            ? null
            : Align(
                widthFactor: 1.0,
                heightFactor: 1.0,
                child: SvgPicture.asset(
                  widget.assetName ?? "",
                  color: AppColors.background,
                  width: 24,
                  height: 24,
                ),
              ),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        filled: true,
        fillColor: AppColors.lightBlue,
        border: styelBorder,
        hintText: widget.text,
        hintStyle: const TextStyle(color: AppColors.background),
        suffixIcon: widget.isPassword
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                child: widget.suffixIcon,
              )
            : null,
        // label: Text(
        //   text,
        //   style: const TextStyle(
        //     color: Color.fromARGB(255, 131, 128, 128),
        //     fontSize: 16,
        //     fontWeight: FontWeight.w400,
        //   ),
        // ),
      ),
      onChanged: widget.onChanged,
      maxLength: widget.maxLength,
      validator: widget.validator,
      obscureText: _obscureText,
    );
  }
}

class LoginScreenTheme extends StatelessWidget {
  const LoginScreenTheme({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.menu,
          // color: Theme.of(context).iconTheme.color,
        ),
        ),
      );
  }
}
