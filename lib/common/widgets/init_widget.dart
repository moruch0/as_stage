import 'package:as_stage/common/repository/api.dart';
import 'package:as_stage/common/repository/repo_product.dart';
import 'package:as_stage/common/repository/repo_settings.dart';
import 'package:as_stage/screens/products/src/bloc/bloc_products.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'app_nav_bar/view_model.dart';

class InitWidget extends StatelessWidget {
  const InitWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Api(),
        ),
        RepositoryProvider(
          create: (context) => RepoSettings(),
        ),
        RepositoryProvider(
          create: (context) => RepoProduct(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        ChangeNotifierProvider<NavBarViewModel>(
          create: (_) => NavBarViewModel()..init(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => BlocProducts(
              repo: RepositoryProvider.of<RepoProduct>(context),
            )..add(EventGetProducts()),
          ),
        ],
        child: child,
      ),
    );
  }
}
