import 'package:as_stage/common/models/response/products_model.dart';
import 'package:flutter/material.dart';

class ProductsVModel extends ChangeNotifier {
  late List<ProductModel> _products;
  late List<ProductModel> _ratingProducts;
  late List<String> _categories;
  late String _selectedCategorie;
  late String _sort;
  late double _rating;

  void init() {
    _products = [];
    _categories = [];
    _selectedCategorie = "Categorie";
    _sort = "desc";
    _rating = 0;

    _ratingProducts = [];
  }

  List<ProductModel> get products => _products;
  List<ProductModel> get ratingProducts => _ratingProducts;

  List<String> get categories => _categories;
  String get selectedCategorie => _selectedCategorie;
  String get sort => _sort;
  String get rating => _rating.toString();

  set products(List<ProductModel> products) {
    _products = products;
    _ratingProducts = products;
    notifyListeners();
  }

  set categories(List<String> categories) {
    _categories = categories;
    notifyListeners();
  }

  set selectedCategorie(String selected) {
    _selectedCategorie = selected;
    notifyListeners();
  }

  set sort(String sort) {
    _sort = sort;
    notifyListeners();
  }

  setRating(String raiting) {
    _rating = double.parse(raiting);

    if (_rating != 0.0) {
      List<ProductModel> _list = [];

      for (var item in products) {
        var firstNumber = item.rating?.rate?.toString().split(".").first;
        var twoNumber = _rating.toString().split(".").first;

        if (firstNumber == twoNumber) {
          _list.add(item);
        }
      }

      _ratingProducts = _list;
    } else {
      _ratingProducts = products;
    }

    notifyListeners();
  }
}
