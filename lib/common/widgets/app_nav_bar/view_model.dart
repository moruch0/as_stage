import 'package:as_stage/constans/app_assets.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:flutter/cupertino.dart';

enum NavBarModelType { products, settings }

class NavBarViewModel extends ChangeNotifier {
  late int _index;
  late final List<GlobalKey<NavigatorState>> keys;
  late final List<NavBarModel> models;

  void init() {
    _index = 0;

    keys = [
      GlobalKey<NavigatorState>(),
      GlobalKey<NavigatorState>(),
    ];

    models = [
      NavBarModel(
        type: NavBarModelType.products,
        svgImgPath: AppAssets.svg.hanger,
      ),
      NavBarModel(
        type: NavBarModelType.settings,
        svgImgPath: AppAssets.svg.settings,
      ),
    ];

    models[_index].isActive = true;
  }

  int get index => _index;

  set index(int value) {
    if (_index != value) {
      models[_index].isActive = false;

      _index = value;

      models[_index].isActive = true;

      notifyListeners();
    }
  }

  void changeBadgeValue({
    required NavBarModelType type,
    required bool value,
  }) {
    switch (type) {
      case NavBarModelType.products:
        models
            .firstWhere((e) => e.type == NavBarModelType.products)
            .isBadged = value;
        break;
      case NavBarModelType.settings:
        models.firstWhere((e) => e.type == NavBarModelType.settings).isBadged =
            value;
        break;
    }

    notifyListeners();
  }
}

class NavBarModel {
  final NavBarModelType type;
  final String svgImgPath;
  bool isBadged;
  bool isActive;

  NavBarModel({
    required this.svgImgPath,
    required this.type,
    this.isActive = false,
    this.isBadged = false,
  });

  String title(S locale) {
    switch (type) {
      case NavBarModelType.products:
        return locale.products;

      case NavBarModelType.settings:
        return locale.settings;
    }
  }
}