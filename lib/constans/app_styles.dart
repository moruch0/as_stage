import 'package:as_stage/constans/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyles {
  static const mediumText = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.primaryText,
  );
  static const additionalText = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.baseColor,
  );
  static const baseStyle = TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w400,
    color: AppColors.background,
  );
  static const primaryText = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: AppColors.background,
  );
  static const topTitle = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.gray_3,
  );
  static const titleText = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w500,
    color: AppColors.background,
  );
  static const label = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );
  static const s14w500 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.black
  );
  static const s16w700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    color: AppColors.black,
  );
}
