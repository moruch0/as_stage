part of 'bloc_products.dart';

abstract class EventBlocProducts {}

class EventGetProducts extends EventBlocProducts {
  final String sort;

  EventGetProducts({this.sort = "desc"});
}

class EventGetCategorieProducts extends EventBlocProducts {
  final String categorie;
  final String sort;
  EventGetCategorieProducts({required this.categorie, required this.sort});
}
