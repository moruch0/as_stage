import 'package:shared_preferences/shared_preferences.dart';

class RepoSettings {
  SharedPreferences? prefs;
  Future<void> init() async {
    prefs = await SharedPreferences.getInstance();
  }
  Future<bool?> saveLocal(String locale) async {
    if (prefs == null) return false;
    return prefs?.setString('locale', locale);
  }
  Future<String?> readLocal() async {
     if (prefs == null) return null;
    return prefs?.getString('locale');
  }
}