import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/constans/app_styles.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:as_stage/screens/products/src/bloc/bloc_products.dart';
import 'package:as_stage/screens/products/src/vmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/product_widget.dart';

class ProductsScreen extends StatelessWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vm = context.watch<ProductsVModel>();
    final watchVM = context.watch<ProductsVModel>();
    return Scaffold(
      backgroundColor: AppColors.lightPrimary,
      appBar: AppBar(
        backgroundColor: AppColors.darkBlue,
        title: Text(
          S.of(context).products,
          style: AppStyles.titleText,
        ),
      ),
      body: BlocConsumer<BlocProducts, StateBlocProducts>(
          listener: ((context, state) {
        if (state is StateProductsData) vm.products = state.data;
        if (state is StateCategoriesData) {
          vm.categories = state.data;
        }
      }), builder: (context, state) {
        if (state is StateProductsData) {
          return ListView(
            padding: const EdgeInsets.all(10),
            children: [
              Column(
                children: [
                  DropdownButton(
                    isExpanded: true,
                    hint: Text(watchVM.sort),
                    items: const [
                      DropdownMenuItem(
                        value: 'asc',
                        child: Text("asc"),
                      ),
                      DropdownMenuItem(
                        value: 'desc',
                        child: Text("desc"),
                      ),
                    ],
                    onChanged: (String? value) async {
                      if (value == null) return;
                      vm.sort = value;
                      if (watchVM.selectedCategorie == "Categorie") {
                        context.read<BlocProducts>().add(
                              EventGetProducts(sort: vm.sort),
                            );
                      } else {
                        context.read<BlocProducts>().add(
                            EventGetCategorieProducts(
                                categorie: vm.selectedCategorie,
                                sort: vm.sort));
                      }
                    },
                  ),
                  Row(
                    children: [
                      DropdownButton(
                        hint: Text(watchVM.selectedCategorie),
                        items: vm.categories
                            .map(
                              (e) => DropdownMenuItem(
                                value: e,
                                child: Text(e),
                              ),
                            )
                            .toList(),
                        onChanged: (String? value) {
                          if (value == null) return;

                          vm.selectedCategorie = value;

                          context.read<BlocProducts>().add(
                              EventGetCategorieProducts(
                                  categorie: vm.selectedCategorie,
                                  sort: vm.sort));
                        },
                      ),
                      DropdownButton(
                        hint: Text(
                            "Rating: ${watchVM.rating == "0.0" ? 'all' : watchVM.rating}"),
                        items: const [
                          DropdownMenuItem(
                            value: '0.0',
                            child: Text("rating: all"),
                          ),
                          DropdownMenuItem(
                            value: '1.0',
                            child: Text("rating:1"),
                          ),
                          DropdownMenuItem(
                            value: '2.0',
                            child: Text("rating:2"),
                          ),
                          DropdownMenuItem(
                            value: '3.0',
                            child: Text("rating:3"),
                          ),
                          DropdownMenuItem(
                            value: '4.0',
                            child: Text("rating:4"),
                          ),
                          DropdownMenuItem(
                            value: '5.0',
                            child: Text("rating:5"),
                          ),
                        ],
                        onChanged: (String? value) {
                          if (value == null) return;

                          vm.setRating(value);
                        },
                      ),
                    ],
                  ),
                ],
              ),
              ...vm.ratingProducts
                  .map(
                    (e) => ProductWidget(
                      product: e,
                    ),
                  )
                  .toList(),
            ],
          );
        }

        return const Center(
          child: CircularProgressIndicator(
            color: AppColors.lightBlue,
          ),
        );
      }),
    );
  }
}
