import 'package:as_stage/screens/settings/src/settings_screen.dart';
import 'package:flutter/material.dart';

MaterialPageRoute settingsRoute() {
  return MaterialPageRoute(builder: (contextRoute) {
    return const SettingsFeature();
  });
}

class SettingsFeature extends StatelessWidget {
  const SettingsFeature({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SettingsScreen();
  }
}