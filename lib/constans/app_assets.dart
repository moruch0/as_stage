abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

final String splashScreen = 'assets/images/bitmap/splash_screen.png';
final String logo = 'assets/images/bitmap/logo.png';
final String jacket = 'assets/imagees/bitmap/jacket.png';
}

class _Svg {
  const _Svg();
  final String account = 'assets/images/svg/account.svg';
  final String password = 'assets/images/svg/password.svg';
  final String obscure = 'assets/images/svg/obscure.svg';
  final String settings = 'assets/images/svg/settings.svg';
  final String hanger = 'assets/images/svg/hanger.svg';
  final String arrow = 'assets/images/svg/arrow.svg';
}