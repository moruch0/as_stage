import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:as_stage/screens/products/feature.dart';
import 'package:as_stage/screens/settings/feature.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'view_model.dart';

class AppNavBar extends StatelessWidget {
  const AppNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: context.watch<NavBarViewModel>().index,
        children: [
          Navigator(
            key: context.watch<NavBarViewModel>().keys[0],
            onGenerateRoute: (settings) => produtsRoute(),
          ),
          Navigator(
              key: context.watch<NavBarViewModel>().keys[1],
              onGenerateRoute: (settings) => settingsRoute()),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: AppColors.lightPrimary,
        elevation: 0,
        currentIndex: context.watch<NavBarViewModel>().index,
        selectedItemColor: AppColors.darkBlue,
        unselectedItemColor: AppColors.gray_3,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        onTap: (index) => context.read<NavBarViewModel>().index = index,
        items: context
            .read<NavBarViewModel>()
            .models
            .map(
              (e) => BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  e.svgImgPath,
                  color: e.isActive ? AppColors.darkBlue : AppColors.gray_3,
                  width: 37,
                  height: 37,
                ),
                label: e.title(S.of(context)),
              ),
            )
            .toList(),
      ),
    );
  }
}
