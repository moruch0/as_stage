import 'dart:ui';

class AppColors{
  static const primary = Color(0xFF22A2BD);
  static const primaryText = Color(0xFF000000);
  static const bottomNavigationBar = Color(0xFFFFFFFF);
  static const background = Color(0xFFffffff);
  static const basicInactiveTab = Color(0xFFA3A3A3);
  static const gray_3 = Color(0xFF828282);
  static const gray_4 = Color(0xFFBDBDBD);
  static const black = Color(0xFF000000);
  static const baseColor = Color(0xFF6667AB);
  static const backgroundColor = Color(0xFFe7cef0);
  static const addColor = Color(0xFF9F77C7);
  static const darkBlue = Color(0xFF557A95);
  static const lightBlue = Color(0xFF7395AE);
  static const brown = Color(0xFFB1A296);
  static const darkBrown = Color(0xFF5D5C61);
  static const lightPrimary = Color(0xFFCFD8DC);
}