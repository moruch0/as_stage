import 'package:as_stage/common/models/response/products_model.dart';
import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/constans/app_styles.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:flutter/material.dart';

class ProductDetailsScreen extends StatelessWidget {
  const ProductDetailsScreen({Key? key, required this.product})
      : super(key: key);
  final ProductModel product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: AppBar(
        backgroundColor: AppColors.darkBlue,
        // automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).product_details,
          style: AppStyles.titleText,
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 33),
        children: [
          Text(
            product.title ?? "",
            textAlign: TextAlign.center,
            style: AppStyles.s14w500,
          ),
          const SizedBox(height: 21),
          Image.network(
            product.image ?? "",
            width: 216,
            height: 318,
          ),
          const SizedBox(height: 29),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Text(
                  product.rating?.rate.toString() ?? "",
                  style: AppStyles.mediumText,
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                decoration: BoxDecoration(
                    color: AppColors.lightPrimary,
                    borderRadius: BorderRadius.circular(10)),
              ),
              Text(
                '\$${product.price}',
                style: AppStyles.s16w700,
              ),
            ],
          ),
          const SizedBox(height: 40),
          Text(
            product.description ?? "",
            textAlign: TextAlign.center,
            style: AppStyles.s14w500,
          )
        ],
      ),
    );
  }
}
