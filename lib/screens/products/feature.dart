import 'package:as_stage/screens/products/src/products.dart';
import 'package:as_stage/screens/products/src/vmodel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


MaterialPageRoute produtsRoute() {
  return MaterialPageRoute(builder: (contextRoute) {
    return const ProductsFeature();
  });
}

class ProductsFeature extends StatelessWidget {
  const ProductsFeature({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  ChangeNotifierProvider<ProductsVModel>(
      create: (_) => ProductsVModel()..init(),
      child: const ProductsScreen(),
    );
  }
}
