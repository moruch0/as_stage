import 'package:as_stage/common/models/response/products_model.dart';
import 'package:as_stage/common/repository/repo_product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'states.dart';
part 'events.dart';

class BlocProducts extends Bloc<EventBlocProducts, StateBlocProducts> {
  final RepoProduct repo;
  BlocProducts({
    required this.repo,
  }) : super(StateProductsInitial()) {
    on<EventGetProducts>((event, emit) async {
      emit(StateProductsLoading(true));
      final resultProduct = await repo.getProducts(sort: event.sort);
      final resultCategories = await repo.getAllCategories();

      if (resultProduct.errorMesage != null) {
        emit(
          StateProductsError(resultProduct.errorMesage!),
        );
        return;
      }

      if (resultCategories.errorMesage != null) {
        emit(
          StateProductsError(resultCategories.errorMesage!),
        );
        return;
      }

      emit(StateProductsLoading(false));
      emit(StateCategoriesData(data: resultCategories.categories ?? []));
      emit(StateProductsData(data: resultProduct.products ?? []));
    });

    on<EventGetCategorieProducts>((event, emit) async {
      emit(StateProductsLoading(true));
      final resultProduct = await repo.getCategorieProducts(
          categorie: event.categorie, sort: event.sort);

      if (resultProduct.errorMesage != null) {
        emit(
          StateProductsError(resultProduct.errorMesage!),
        );
        return;
      }

      emit(StateProductsLoading(false));
      emit(StateProductsData(data: resultProduct.products ?? []));
    });
  }
}
