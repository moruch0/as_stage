import 'package:as_stage/common/repository/repo_settings.dart';
import 'package:as_stage/common/widgets/app_nav_bar/app_nav_bar.dart';
import 'package:as_stage/constans/app_assets.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:as_stage/screens/login_screen/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    final repoSettings = Provider.of<RepoSettings>(
      context,
      listen: false,
    );
    repoSettings.init().whenComplete(
      () async {
        var defaultLocale = const Locale('ru', 'RU');
        final locale = await repoSettings.readLocal();
        if (locale == "en") {
          defaultLocale = const Locale('en');
        }
        S.load(defaultLocale).whenComplete(
              () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => const LoginScreen(),
                ),
              ),
            );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Image.asset(
        AppAssets.images.splashScreen,
        fit: BoxFit.fill,
      ),
    );
  }
}
