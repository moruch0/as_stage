import 'package:as_stage/common/repository/repo_settings.dart';
import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/constans/app_styles.dart';
import 'package:as_stage/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightPrimary,
      appBar: AppBar(
        backgroundColor: AppColors.darkBlue,
        automaticallyImplyLeading: false,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).settings,
          style: Theme.of(context)
          .textTheme
          .headline6
          ?.copyWith(color: AppColors.background)
          ,
        ),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${S.of(context).language}: ",
              style: const TextStyle(
                fontSize: 18,
              ),
            ),
            DropdownButton(
              value: Intl.getCurrentLocale(),
              items: [
                DropdownMenuItem(
                  value: 'en',
                  child: Text(S.of(context).english),
                ),
                DropdownMenuItem(
                  value: 'ru_RU',
                  child: Text(S.of(context).russian),
                ),
              ],
              onChanged: (String? value) async {
                if (value == null) return;
                if (value == 'en') {
                  await S.load(const Locale('en'));
                  setState(() {});
                } else if (value == 'ru_RU') {
                  await S.load(const Locale('ru', 'RU'));
                  setState(() {});
                }
                final repoSettings =
                    Provider.of<RepoSettings>(context, listen: false);

                repoSettings.saveLocal(value);
              },
            ),
          ],
        ),
      ),
    );
  }
}