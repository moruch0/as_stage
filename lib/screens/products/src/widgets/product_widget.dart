import 'package:as_stage/common/models/response/products_model.dart';
import 'package:as_stage/constans/app_assets.dart';
import 'package:as_stage/constans/app_colors.dart';
import 'package:as_stage/constans/app_styles.dart';
import 'package:as_stage/screens/product_screen/product_details_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductWidget extends StatelessWidget {
  final ProductModel product;
  const ProductWidget({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
              builder: (context) => ProductDetailsScreen(
                    product: product,
                  )),
        );
      },
      child: Container(
        decoration: const BoxDecoration(
            color: AppColors.lightBlue,
            borderRadius: BorderRadius.all(Radius.circular(7))),
        margin: const EdgeInsets.only(bottom: 11),
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 9),
        child: Row(
          children: [
            CircleAvatar(
              radius: 39,
              backgroundColor: AppColors.darkBrown,
              child: CircleAvatar(
                radius: 38,
                backgroundImage:
                    product.image == null ? null : NetworkImage(product.image!),
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    child: Text(
                      product.title ?? "",
                      overflow: TextOverflow.ellipsis,
                      style: AppStyles.primaryText,
                      maxLines: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Price: \$ ${product.price}",
                        style: AppStyles.primaryText,
                      ),
                      Text(
                        "Rating: ${product.rating?.rate}",
                        style: AppStyles.primaryText,
                      )
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              width: 3,
            ),
            SvgPicture.asset(
              AppAssets.svg.arrow,
              height: 19,
              width: 10,
              fit: BoxFit.scaleDown,
              color: AppColors.background,
            ),
          ],
        ),
      ),
    );
  }
}
