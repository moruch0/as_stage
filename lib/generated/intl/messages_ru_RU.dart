// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "authorization": MessageLookupByLibrary.simpleMessage("Авторизация"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "counter_value":
            MessageLookupByLibrary.simpleMessage("Значение Счётчика:"),
        "create": MessageLookupByLibrary.simpleMessage("Создать"),
        "dont_have_account":
            MessageLookupByLibrary.simpleMessage("У вас еще нет аккаунта?"),
        "english": MessageLookupByLibrary.simpleMessage("Английский"),
        "enter_login": MessageLookupByLibrary.simpleMessage("Введите логин"),
        "enter_login_and_pass":
            MessageLookupByLibrary.simpleMessage("Введите логин и пороль"),
        "enter_the_password":
            MessageLookupByLibrary.simpleMessage("Введите пароль"),
        "error": MessageLookupByLibrary.simpleMessage("Oшибка"),
        "home_page": MessageLookupByLibrary.simpleMessage("Домашняя страница"),
        "invalid_username_or_password_entered":
            MessageLookupByLibrary.simpleMessage(
                "Введен  неверные логин или пароль"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "login": MessageLookupByLibrary.simpleMessage("Вход"),
        "login_must_contain_least_3_characters":
            MessageLookupByLibrary.simpleMessage(
                "Логин должен содержать не менее 3 символов"),
        "ok": MessageLookupByLibrary.simpleMessage("Ok"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "password_must_contain_least_8_characters":
            MessageLookupByLibrary.simpleMessage(
                "Пароль должен содержать не менее 8 символов"),
        "product_details":
            MessageLookupByLibrary.simpleMessage("Подробная информация"),
        "products": MessageLookupByLibrary.simpleMessage("Продукты"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Что-то пошло не так "),
        "try_again": MessageLookupByLibrary.simpleMessage("Попробуйте снова"),
        "username": MessageLookupByLibrary.simpleMessage("Логин")
      };
}
